/*jslint devel: true */
/*jslint browser: true*/ /*global  $*/
//declaration
var start = 0;
var c = 0;
var score = 0;
var hiscore = 0;
var gameOver = 0;
var expr = 4;
var arrow = 4;
var user = {};
//-----------------------------------------------
var canvas = document.querySelector('canvas');
canvas.width = 350;
canvas.height = 550;
var ctx = canvas.getContext('2d');
function Canvas() {
    "use strict";
    ctx.fillStyle = "#000000";
    ctx.fillRect(0, 0, 350, 550);
}
//---------Lapin ----------------------------------------
var lapin = { x: 150, y: 500, speed: 350 };
var rabbit = new Image();
rabbit.src = './img/rabbit.png';
rabbit.width = 40;
rabbit.height = 40;
var step_rabbit = document.createElement("audio");
/*
step_rabbit.src = "./audio/step.ogg";
step_rabbit.volume = 0.2;
*/
function Lapin() {
    "use strict";
    ctx.drawImage(rabbit, (lapin.x) + 5, (lapin.y) + 5, rabbit.width, rabbit.height);
}
//------carotte -----------
var carotte = { x: 0, y: 0 };
var carrot = new Image();
carrot.src = './img/carotte.png';
carrot.width = 30;
carrot.height = 30;
function Carotte() {
    "use strict";
    ctx.drawImage(carrot, (carotte.x + 10), (carotte.y + 10), carrot.width, carrot.height);
}
//------------Banane ---------
var banane = new Image();
banane.src = './img/banane.png';
banane.width = 30;
banane.height = 30;
var banane1 = { x: 50, y: 450 };
var banane2 = { x: 250, y: 50 };
function Banane1() {
    "use strict";
    ctx.fillStyle = "#322C1C";
    ctx.fillRect(50, 450, 50, 50);
    ctx.strokeStyle = "#FFFC79";
    ctx.strokeRect(52, 452, 46, 46);
    ctx.lineWidth = 4;
    ctx.drawImage(banane, (banane1.x) + 10, (banane1.y) + 10, banane.width, banane.height);
}
function Banane2() {
    "use strict";
    ctx.fillStyle = "#322C1C";
    ctx.fillRect(250, 50, 50, 50);
    ctx.strokeStyle = "#FFFC79";
    ctx.strokeRect(252, 52, 46, 46);
    ctx.lineWidth = 4;
    ctx.drawImage(banane, (banane2.x) + 10, (banane2.y) + 10, banane.width, banane.height);
}

//-------------------Cerise----------------------------------
var cerise = new Image();
cerise.src = './img/cerise.png';
cerise.width = 24;
cerise.height = 24;
var cerise1 = { x: 50, y: 50 };
var cerise2 = { x: 250, y: 450 };
function Cerise1() {
    "use strict";
    ctx.fillStyle = "#260D17";
    ctx.fillRect(50, 50, 50, 50);
    ctx.strokeStyle = "#CC1F32";
    ctx.strokeRect(52, 52, 46, 46);
    ctx.lineWidth = 4;
    ctx.drawImage(cerise, (cerise1.x) + 13, (cerise1.y) + 12, cerise.width, cerise.height);
}
function Cerise2() {
    "use strict";
    ctx.fillStyle = "#260D17";
    ctx.fillRect(250, 450, 50, 50);
    ctx.strokeStyle = "#CC1F32";
    ctx.strokeRect(252, 452, 46, 46);
    ctx.lineWidth = 4;
    ctx.drawImage(cerise, (cerise2.x) + 13, (cerise2.y) + 12, cerise.width, cerise.height);
}
//----------- Poire ----------------------------------------
var poire = new Image();
poire.src = './img/poire.png';
poire.width = 24;
poire.height = 24;
var poire1 = { x: 150, y: 50 };
var poire2 = { x: 150, y: 450 };
function Poire1() {
    "use strict";
    ctx.fillStyle = "#353706";
    ctx.fillRect(150, 50, 50, 50);
    ctx.strokeStyle = "#D7E934";
    ctx.strokeRect(152, 52, 46, 46);
    ctx.lineWidth = 4;
    ctx.drawImage(poire, (poire1.x) + 13, (poire1.y) + 11, poire.width, poire.height);
}
function Poire2() {
    "use strict";
    ctx.fillStyle = "#353706";
    ctx.fillRect(150, 450, 50, 50);
    ctx.strokeStyle = "#D7E934";
    ctx.strokeRect(152, 452, 46, 46);
    ctx.lineWidth = 4;
    ctx.drawImage(poire, (poire2.x) + 13, (poire2.y) + 11, poire.width, poire.height);
}

//----------------------- Ananas ----------------------------
var pineapple = new Image();
pineapple.src = './img/pineapple.png';
pineapple.width = 30;
pineapple.height = 30;
var pineapple1 = { x: 150, y: 100 };
var pineapple2 = { x: 150, y: 400 };
function Pineapple1() {
    "use strict";
    ctx.fillStyle = "#322401";
    ctx.fillRect(150, 100, 50, 50);
    ctx.strokeStyle = "#C98A00";
    ctx.strokeRect(152, 102, 46, 46);
    ctx.lineWidth = 4;
    ctx.drawImage(pineapple, (pineapple1.x) + 10, (pineapple1.y) + 10, pineapple.width, pineapple.height);
}
function Pineapple2() {
    "use strict";
    ctx.fillStyle = "#322401";
    ctx.fillRect(150, 400, 50, 50);
    ctx.strokeStyle = "#C98A00";
    ctx.strokeRect(152, 402, 46, 46);
    ctx.lineWidth = 4;
    ctx.drawImage(pineapple, (pineapple2.x) + 10, (pineapple2.y) + 10, pineapple.width, pineapple.height);
}

//-------------Raisin-----------------------
var raisin = new Image();
raisin.src = './img/raisin.png';
raisin.width = 30;
raisin.height = 30;
var raisin1 = { x: 250, y: 350 };
var raisin2 = { x: 50, y: 150 };
function Raisin1() {
    "use strict";
    ctx.fillStyle = "#0f1f24";
    ctx.fillRect(250, 350, 50, 50);
    ctx.strokeStyle = "#7900b1";
    ctx.strokeRect(252, 352, 46, 46);
    ctx.lineWidth = 4;
    ctx.drawImage(raisin, (raisin1.x) + 10, (raisin1.y) + 10, raisin.width, raisin.height);
}
function Raisin2() {
    "use strict";
    ctx.fillStyle = "#0f1f24";
    ctx.fillRect(50, 150, 50, 50);
    ctx.strokeStyle = "#7900b1";
    ctx.strokeRect(52, 152, 46, 46);
    ctx.lineWidth = 4;
    ctx.drawImage(raisin, (raisin2.x) + 10, (raisin2.y) + 10, raisin.width, raisin.height);
}

//------------ Fraise -------------------------------
var fraise = new Image();
fraise.src = './img/fraise.png';
fraise.width = 20;
fraise.height = 20;
var fraise1 = { x: 250, y: 150 };
var fraise2 = { x: 50, y: 350 };
function Fraise1() {
    "use strict";
    ctx.fillStyle = "#320B01";
    ctx.fillRect(50, 350, 50, 50);
    ctx.strokeStyle = "#FF3145";
    ctx.strokeRect(52, 352, 46, 46);
    ctx.lineWidth = 4;
    ctx.drawImage(fraise, (fraise2.x) + 15, (fraise2.y) + 15, fraise.width, fraise.height);
}
function Fraise2() {
    "use strict";
    ctx.fillStyle = "#320B01";
    ctx.fillRect(250, 150, 50, 50);
    ctx.strokeStyle = "#FF3145";
    ctx.strokeRect(252, 152, 46, 46);
    ctx.lineWidth = 4;
    ctx.drawImage(fraise, (fraise1.x) + 15, (fraise1.y) + 15, fraise.width, fraise.height);
}

//-----------Citron ----------------------------
var citron = new Image();
citron.src = './img/citron.png';
citron.width = 20;
citron.height = 20;
var citron1 = { x: 250, y: 250 };
var citron2 = { x: 50, y: 250 };
function Citron1() {
    "use strict";
    ctx.fillStyle = "#403E01";
    ctx.fillRect(50, 250, 50, 50);
    ctx.strokeStyle = "#FFFB00";
    ctx.strokeRect(52, 252, 46, 46);
    ctx.lineWidth = 4;
    ctx.drawImage(citron, (citron2.x) + 15, (citron2.y) + 15, citron.width, citron.height);
}
function Citron2() {
    "use strict";
    ctx.fillStyle = "#403E01";
    ctx.fillRect(250, 250, 50, 50);
    ctx.strokeStyle = "#FFFB00";
    ctx.strokeRect(252, 252, 46, 46);
    ctx.lineWidth = 4;
    ctx.drawImage(citron, (citron1.x) + 15, (citron1.y) + 15, citron.width, citron.height);
}

//---------Pomme-------------------------------
var pomme = new Image();
pomme.src = './img/pomme.png';
pomme.width = 24;
pomme.height = 24;
var pomme1 = { x: 150, y: 300 };
var pomme2 = { x: 150, y: 200 };
function Pomme1() {
    "use strict";
    ctx.fillStyle = "#290000";
    ctx.fillRect(150, 200, 50, 50);
    ctx.strokeStyle = "#be0000";
    ctx.strokeRect(152, 202, 46, 46);
    ctx.lineWidth = 4;
    ctx.drawImage(pomme, (pomme2.x) + 13, (pomme2.y) + 12, pomme.width, pomme.height);
}
function Pomme2() {
    "use strict";
    ctx.fillStyle = "#290000";
    ctx.fillRect(150, 300, 50, 50);
    ctx.strokeStyle = "#be0000";
    ctx.strokeRect(152, 302, 46, 46);
    ctx.lineWidth = 4;
    ctx.drawImage(pomme, (pomme1.x) + 13, (pomme1.y) + 12, pomme.width, pomme.height);
}
//------renard ------------
var renard = { x: 150, y: 0, speed: 400 };
var fox = new Image();
fox.src = './img/fox.png';
fox.width = 40;
fox.height = 40;
function Renard() {
    "use strict";
    ctx.drawImage(fox, (renard.x) + 5, (renard.y) + 5, fox.width, fox.height);
}
//-------------------Mouvement du renard ----------------------------------------------
function Choice() {
    "use strict";
    var Lx = (lapin.x) / 50;
    var Ly = (lapin.y) / 50;
    var Rx = (renard.x) / 50;
    var Ry = (renard.y) / 50;
    var d_RL_1move_onLeft = Math.sqrt((Lx - Rx + 1) * (Lx - Rx + 1) + (Ly - Ry) * (Ly - Ry));
    var d_RL_1move_onRight = Math.sqrt((Lx - Rx - 1) * (Lx - Rx - 1) + (Ly - Ry) * (Ly - Ry));
    var d_RL_1move_onUp = Math.sqrt((Ly - Ry + 1) * (Ly - Ry + 1) + (Lx - Rx) * (Lx - Rx));
    var d_RL_1move_onDown = Math.sqrt((Ly - Ry - 1) * (Ly - Ry - 1) + (Lx - Rx) * (Lx - Rx));
    var choice = Math.min(d_RL_1move_onLeft, d_RL_1move_onRight, d_RL_1move_onUp, d_RL_1move_onDown);
    if ((Lx < 350) && (Ly < 550)) {
        if (choice === d_RL_1move_onLeft) {
            expr = 0;
            FoxMove();
        } else if (choice === d_RL_1move_onRight) {
            expr = 1;
            FoxMove();
        } else if (choice === d_RL_1move_onUp) {
            expr = 2;
            FoxMove();
        } else if (choice === d_RL_1move_onDown) {
            expr = 3;
            FoxMove();
        }
    }
}
setInterval(Choice, renard.speed);


//------------------------------------------------------------------------------------------
function FoxMove() {
    "use strict";
    switch (expr) {
        case 0:
            if (renard.x > 0) {
                if ((renard.x === (cerise1.x + 50) && renard.y === cerise1.y)
                    || (renard.x === (cerise2.x + 50) && renard.y === cerise2.y)
                    || (renard.x === (banane1.x + 50) && renard.y === banane1.y)
                    || (renard.x === (banane2.x + 50) && renard.y === banane2.y)
                    || (renard.x === (poire1.x + 50) && renard.y === poire1.y)
                    || (renard.x === (poire2.x + 50) && renard.y === poire2.y)
                    || (renard.x === (pineapple1.x + 50) && renard.y === pineapple1.y)
                    || (renard.x === (pineapple2.x + 50) && renard.y === pineapple2.y)
                    || (renard.x === (raisin1.x + 50) && renard.y === raisin1.y)
                    || (renard.x === (raisin2.x + 50) && renard.y === raisin2.y)
                    || (renard.x === (fraise1.x + 50) && renard.y === fraise1.y)
                    || (renard.x === (fraise2.x + 50) && renard.y === fraise2.y)
                    || (renard.x === (citron1.x + 50) && renard.y === citron1.y)
                    || (renard.x === (citron2.x + 50) && renard.y === citron2.y)
                    || (renard.x === (pomme1.x + 50) && renard.y === pomme1.y)
                    || (renard.x === (pomme2.x + 50) && renard.y === pomme2.y)) {
                    expr = Math.floor(Math.random() * 4);
                    FoxMove();
                } else {
                    renard.x -= 50;
                }
            }
            break;
        case 1:
            if (renard.x < 300) {
                if ((renard.x === (cerise1.x - 50) && renard.y === cerise1.y)
                    || (renard.x === (cerise2.x - 50) && renard.y === cerise2.y)
                    || (renard.x === (banane1.x - 50) && renard.y === banane1.y)
                    || (renard.x === (banane2.x - 50) && renard.y === banane2.y)
                    || (renard.x === (poire1.x - 50) && renard.y === poire1.y)
                    || (renard.x === (poire2.x - 50) && renard.y === poire2.y)
                    || (renard.x === (pineapple1.x - 50) && renard.y === pineapple1.y)
                    || (renard.x === (pineapple2.x - 50) && renard.y === pineapple2.y)
                    || (renard.x === (raisin1.x - 50) && renard.y === raisin1.y)
                    || (renard.x === (raisin2.x - 50) && renard.y === raisin2.y)
                    || (renard.x === (fraise1.x - 50) && renard.y === fraise1.y)
                    || (renard.x === (fraise2.x - 50) && renard.y === fraise2.y)
                    || (renard.x === (citron1.x - 50) && renard.y === citron1.y)
                    || (renard.x === (citron2.x - 50) && renard.y === citron2.y)
                    || (renard.x === (pomme1.x - 50) && renard.y === pomme1.y)
                    || (renard.x === (pomme2.x - 50) && renard.y === pomme2.y)) {
                    expr = Math.floor(Math.random() * 4);
                    FoxMove();
                } else {
                    renard.x += 50;
                }
            }
            break;
        case 2:
            if (renard.y > 0) {
                if ((renard.y === (cerise1.y + 50) && renard.x === cerise1.x)
                    || (renard.y === (cerise2.y + 50) && renard.x === cerise2.x)
                    || (renard.y === (banane1.y + 50) && renard.x === banane1.x)
                    || (renard.y === (banane2.y + 50) && renard.x === banane2.x)
                    || (renard.y === (poire1.y + 50) && renard.x === poire1.x)
                    || (renard.y === (poire2.y + 50) && renard.x === poire2.x)
                    || (renard.y === (pineapple1.y + 50) && renard.x === pineapple1.x)
                    || (renard.y === (pineapple2.y + 50) && renard.x === pineapple2.x)
                    || (renard.y === (raisin1.y + 50) && renard.x === raisin1.x)
                    || (renard.y === (raisin2.y + 50) && renard.x === raisin2.x)
                    || (renard.y === (fraise1.y + 50) && renard.x === fraise1.x)
                    || (renard.y === (fraise2.y + 50) && renard.x === fraise2.x)
                    || (renard.y === (citron1.y + 50) && renard.x === citron1.x)
                    || (renard.y === (citron2.y + 50) && renard.x === citron2.x)
                    || (renard.y === (pomme1.y + 50) && renard.x === pomme1.x)
                    || (renard.y === (pomme2.y + 50) && renard.x === pomme2.x)) {
                    expr = Math.floor(Math.random() * 4);
                    FoxMove();
                } else {
                    renard.y -= 50;
                }
            }
            break;
        case 3:
            if (renard.y < 500) {
                if ((renard.y === (cerise1.y - 50) && renard.x === cerise1.x)
                    || (renard.y === (cerise2.y - 50) && renard.x === cerise2.x)
                    || (renard.y === (banane1.y - 50) && renard.x === banane1.x)
                    || (renard.y === (banane2.y - 50) && renard.x === banane2.x)
                    || (renard.y === (poire1.y - 50) && renard.x === poire1.x)
                    || (renard.y === (poire2.y - 50) && renard.x === poire2.x)
                    || (renard.y === (pineapple1.y - 50) && renard.x === pineapple1.x)
                    || (renard.y === (pineapple2.y - 50) && renard.x === pineapple2.x)
                    || (renard.y === (raisin1.y - 50) && renard.x === raisin1.x)
                    || (renard.y === (raisin2.y - 50) && renard.x === raisin2.x)
                    || (renard.y === (fraise1.y - 50) && renard.x === fraise1.x)
                    || (renard.y === (fraise2.y - 50) && renard.x === fraise2.x)
                    || (renard.y === (citron1.y - 50) && renard.x === citron1.x)
                    || (renard.y === (citron2.y - 50) && renard.x === citron2.x)
                    || (renard.y === (pomme1.y - 50) && renard.x === pomme1.x)
                    || (renard.y === (pomme2.y - 50) && renard.x === pomme2.x)) {
                    expr = Math.floor(Math.random() * 4);
                    FoxMove();
                } else {
                    renard.y += 50;
                }
            }
            break;
    }
}
//------------------ Diriger le Lapin ----------------

//------------------ Tactile ---------------------------------------
function Tactile() {
    "use strict";
    var Lx = ((lapin.x) / 50);
    var Ly = ((lapin.y) / 50);
    var Ux = ((user.x) / 50);
    var Uy = ((user.y) / 50);
    if (Lx - Ux > 0) {
        arrow = 0;
        RabbitMove();
    } else if (Lx - Ux < 0) {
        arrow = 1;
        RabbitMove();
    } else if (Ly - Uy > 0) {
        arrow = 2;
        RabbitMove();
    } else if (Ly - Uy < 0) {
        arrow = 3;
        RabbitMove();
    }
}

function Digitizer() {
    //----------------a--------------------------------
    if (gameOver === 0) {
        $("#aa").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('AA');
            user = { x: 0, y: 0 };
            Tactile();
        });
        $("#ab").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('AB');
            user = { x: 50, y: 0 };
            Tactile();
        });
        $("#ac").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('AC');
            user = { x: 100, y: 0 };
            Tactile();
        });
        $("#ad").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('AD');
            user = { x: 150, y: 0 };
            Tactile();
        });
        $("#ae").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('AE');
            user = { x: 200, y: 0 };
            Tactile();
        });
        $("#af").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('AF');
            user = { x: 250, y: 0 };
            Tactile();
        });
        $("#ag").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('AG');
            user = { x: 300, y: 0 };
            Tactile();
        });
        //-----------------b---------------------------------
        $("#ba").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('BA');
            user = { x: 0, y: 50 };
            Tactile();
        });
        $("#bc").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('BC');
            user = { x: 100, y: 50 };
            Tactile();
        });
        $("#be").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('BE');
            user = { x: 200, y: 50 };
            Tactile();
        });
        $("#bg").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('BG');
            user = { x: 300, y: 50 };
            Tactile();
        });
        //------------------c---------------------------------
        $("#ca").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('CA');
            user = { x: 0, y: 100 };
            Tactile();
        });
        $("#cb").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('CB');
            user = { x: 50, y: 100 };
            Tactile();
        });
        $("#cc").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('CC');
            user = { x: 100, y: 100 };
            Tactile();
        });
        $("#ce").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('CE');
            user = { x: 200, y: 100 };
            Tactile();
        });
        $("#cf").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('CF');
            user = { x: 250, y: 100 };
            Tactile();
        });
        $("#cg").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('CG');
            user = { x: 300, y: 100 };
            Tactile();
        });
        //---------------------d--------------------------------------
        $("#da").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('DA');
            user = { x: 0, y: 150 };
            Tactile();
        });
        $("#dc").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('DC');
            user = { x: 100, y: 150 };
            Tactile();
        });
        $("#dd").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('DD');
            user = { x: 150, y: 150 };
            Tactile();
        });
        $("#de").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('DE');
            user = { x: 200, y: 150 };
            Tactile();
        });
        $("#dg").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('DG');
            user = { x: 300, y: 150 };
            Tactile();
        });
        //-----------------------e---------------------------------
        $("#ea").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('EA');
            user = { x: 0, y: 200 };
            Tactile();
        });
        $("#eb").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('EB');
            user = { x: 50, y: 200 };
            Tactile();
        });
        $("#ec").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('EC');
            user = { x: 100, y: 200 };
            Tactile();
        });
        $("#ee").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('EE');
            user = { x: 200, y: 200 };
            Tactile();
        });
        $("#ef").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('EF');
            user = { x: 250, y: 200 };
            Tactile();
        });
        $("#eg").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('EG');
            user = { x: 300, y: 200 };
            Tactile();
        });
        //---------------------------f----------------------------------------
        $("#fa").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('FA');
            user = { x: 0, y: 250 };
            Tactile();
        });
        $("#fc").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('FC');
            user = { x: 100, y: 250 };
            Tactile();
        });
        $("#fd").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('FD');
            user = { x: 150, y: 250 };
            Tactile();
        });
        $("#fe").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('FE');
            user = { x: 200, y: 250 };
            Tactile();
        });
        $("#fg").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('FG');
            user = { x: 300, y: 250 };
            Tactile();
        });
        //----------------------------g-----------------------------------------
        $("#ga").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('GA');
            user = { x: 0, y: 300 };
            Tactile();
        });
        $("#gb").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('GB');
            user = { x: 50, y: 300 };
            Tactile();
        });
        $("#gc").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('GC');
            user = { x: 100, y: 300 };
            Tactile();
        });
        $("#ge").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('GE');
            user = { x: 200, y: 300 };
            Tactile();
        });
        $("#gf").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('GF');
            user = { x: 250, y: 300 };
            Tactile();
        });
        $("#gg").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('GG');
            user = { x: 300, y: 300 };
            Tactile();
        });
        //---------------------------f----------------------------------------
        $("#ha").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('HA');
            user = { x: 0, y: 350 };
            Tactile();
        });
        $("#hc").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('HC');
            user = { x: 100, y: 350 };
            Tactile();
        });
        $("#hd").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('HD');
            user = { x: 150, y: 350 };
            Tactile();
        });
        $("#he").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('HE');
            user = { x: 200, y: 350 };
            Tactile();
        });
        $("#hg").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('HG');
            user = { x: 300, y: 350 };
            Tactile();
        });
        //------------------I---------------------------------
        $("#ia").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('IA');
            user = { x: 0, y: 400 };
            Tactile();
        });
        $("#ib").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('IB');
            user = { x: 50, y: 400 };
            Tactile();
        });
        $("#ic").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('IC');
            user = { x: 100, y: 400 };
            Tactile();
        });
        $("#ie").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('IE');
            user = { x: 200, y: 400 };
            Tactile();
        });
        $("#if").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('IF');
            user = { x: 250, y: 400 };
            Tactile();
        });
        $("#ig").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('IG');
            user = { x: 300, y: 400 };
            Tactile();
        });
        //-----------------J---------------------------------
        $("#ja").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('JA');
            user = { x: 0, y: 450 };
            Tactile();
        });
        $("#jc").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('JC');
            user = { x: 100, y: 450 };
            Tactile();
        });
        $("#je").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('JE');
            user = { x: 200, y: 450 };
            Tactile();
        });
        $("#jg").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('JG');
            user = { x: 300, y: 450 };
            Tactile();
        });
        //----------------k--------------------------------
        $("#ka").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('KA');
            user = { x: 0, y: 500 };
            Tactile();
        });
        $("#kb").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('KB');
            user = { x: 50, y: 500 };
            Tactile();
        });
        $("#kc").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('KC');
            user = { x: 100, y: 500 };
            Tactile();
        });
        $("#kd").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('KD');
            user = { x: 150, y: 500 };
            Tactile();
        });
        $("#ke").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('KE');
            user = { x: 200, y: 500 };
            Tactile();
        });
        $("#kf").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('KF');
            user = { x: 250, y: 500 };
            Tactile();
        });
        $("#kg").hover(function (event) {
            "use strict";
            event.preventDefault();
            console.log('KG');
            user = { x: 300, y: 500 };
            Tactile();
        });
    }
}
$('#matrice').on('mouseup', function () {
    'use strict';
    console.log('mouseup', Date.now());
    Digitizer()
});


document.addEventListener('keydown', function (event) {
    "use strict";
    event.preventDefault();
    user = {};
    if (!gameOver) {
        if (event.keyCode === 37) {
            arrow = 4;
            RabbitMove();
        } else if (event.keyCode === 39) {
            arrow = 5;
            RabbitMove();
        } else if (event.keyCode === 38) {
            arrow = 6;
            RabbitMove();
        } else if (event.keyCode === 40) {
            arrow = 7;
            RabbitMove();
        }
    }
});

//--------------------Mouvement du Lapin --------------
function RabbitMove() {
    "use strict";
    switch (arrow) {
        case 0:
            if (lapin.x > 0) {
                if ((lapin.x === (cerise1.x + 50) && lapin.y === cerise1.y)
                    || (lapin.x === (cerise2.x + 50) && lapin.y === cerise2.y)
                    || (lapin.x === (banane1.x + 50) && lapin.y === banane1.y)
                    || (lapin.x === (banane2.x + 50) && lapin.y === banane2.y)
                    || (lapin.x === (poire1.x + 50) && lapin.y === poire1.y)
                    || (lapin.x === (poire2.x + 50) && lapin.y === poire2.y)
                    || (lapin.x === (pineapple1.x + 50) && lapin.y === pineapple1.y)
                    || (lapin.x === (pineapple2.x + 50) && lapin.y === pineapple2.y)
                    || (lapin.x === (raisin1.x + 50) && lapin.y === raisin1.y)
                    || (lapin.x === (raisin2.x + 50) && lapin.y === raisin2.y)
                    || (lapin.x === (fraise1.x + 50) && lapin.y === fraise1.y)
                    || (lapin.x === (fraise2.x + 50) && lapin.y === fraise2.y)
                    || (lapin.x === (citron1.x + 50) && lapin.y === citron1.y)
                    || (lapin.x === (citron2.x + 50) && lapin.y === citron2.y)
                    || (lapin.x === (pomme1.x + 50) && lapin.y === pomme1.y)
                    || (lapin.x === (pomme2.x + 50) && lapin.y === pomme2.y)) {
                } else {
                    lapin.x -= 50;
                    //step_rabbit.play();
                }
            }
            break;
        case 1:
            if (lapin.x < 300) {
                if ((lapin.x === (cerise1.x - 50) && lapin.y === cerise1.y)
                    || (lapin.x === (cerise2.x - 50) && lapin.y === cerise2.y)
                    || (lapin.x === (banane1.x - 50) && lapin.y === banane1.y)
                    || (lapin.x === (banane2.x - 50) && lapin.y === banane2.y)
                    || (lapin.x === (poire1.x - 50) && lapin.y === poire1.y)
                    || (lapin.x === (poire2.x - 50) && lapin.y === poire2.y)
                    || (lapin.x === (pineapple1.x - 50) && lapin.y === pineapple1.y)
                    || (lapin.x === (pineapple2.x - 50) && lapin.y === pineapple2.y)
                    || (lapin.x === (raisin1.x - 50) && lapin.y === raisin1.y)
                    || (lapin.x === (raisin2.x - 50) && lapin.y === raisin2.y)
                    || (lapin.x === (fraise1.x - 50) && lapin.y === fraise1.y)
                    || (lapin.x === (fraise2.x - 50) && lapin.y === fraise2.y)
                    || (lapin.x === (citron1.x - 50) && lapin.y === citron1.y)
                    || (lapin.x === (citron2.x - 50) && lapin.y === citron2.y)
                    || (lapin.x === (pomme1.x - 50) && lapin.y === pomme1.y)
                    || (lapin.x === (pomme2.x - 50) && lapin.y === pomme2.y)) {
                } else {
                    lapin.x += 50;
                    //step_rabbit.play();
                }
            }
            break;
        case 2:
            if (lapin.y > 0) {
                if ((lapin.y === (cerise1.y + 50) && lapin.x === cerise1.x)
                    || (lapin.y === (cerise2.y + 50) && lapin.x === cerise2.x)
                    || (lapin.y === (banane1.y + 50) && lapin.x === banane1.x)
                    || (lapin.y === (banane2.y + 50) && lapin.x === banane2.x)
                    || (lapin.y === (poire1.y + 50) && lapin.x === poire1.x)
                    || (lapin.y === (poire2.y + 50) && lapin.x === poire2.x)
                    || (lapin.y === (pineapple1.y + 50) && lapin.x === pineapple1.x)
                    || (lapin.y === (pineapple2.y + 50) && lapin.x === pineapple2.x)
                    || (lapin.y === (raisin1.y + 50) && lapin.x === raisin1.x)
                    || (lapin.y === (raisin2.y + 50) && lapin.x === raisin2.x)
                    || (lapin.y === (fraise1.y + 50) && lapin.x === fraise1.x)
                    || (lapin.y === (fraise2.y + 50) && lapin.x === fraise2.x)
                    || (lapin.y === (citron1.y + 50) && lapin.x === citron1.x)
                    || (lapin.y === (citron2.y + 50) && lapin.x === citron2.x)
                    || (lapin.y === (pomme1.y + 50) && lapin.x === pomme1.x)
                    || (lapin.y === (pomme2.y + 50) && lapin.x === pomme2.x)) {
                } else {
                    lapin.y -= 50;
                    //step_rabbit.play();
                }
            }
            break;
        case 3:
            if (lapin.y < 500) {
                if ((lapin.y === (cerise1.y - 50) && lapin.x === cerise1.x)
                    || (lapin.y === (cerise2.y - 50) && lapin.x === cerise2.x)
                    || (lapin.y === (banane1.y - 50) && lapin.x === banane1.x)
                    || (lapin.y === (banane2.y - 50) && lapin.x === banane2.x)
                    || (lapin.y === (poire1.y - 50) && lapin.x === poire1.x)
                    || (lapin.y === (poire2.y - 50) && lapin.x === poire2.x)
                    || (lapin.y === (pineapple1.y - 50) && lapin.x === pineapple1.x)
                    || (lapin.y === (pineapple2.y - 50) && lapin.x === pineapple2.x)
                    || (lapin.y === (raisin1.y - 50) && lapin.x === raisin1.x)
                    || (lapin.y === (raisin2.y - 50) && lapin.x === raisin2.x)
                    || (lapin.y === (fraise1.y - 50) && lapin.x === fraise1.x)
                    || (lapin.y === (fraise2.y - 50) && lapin.x === fraise2.x)
                    || (lapin.y === (citron1.y - 50) && lapin.x === citron1.x)
                    || (lapin.y === (citron2.y - 50) && lapin.x === citron2.x)
                    || (lapin.y === (pomme1.y - 50) && lapin.x === pomme1.x)
                    || (lapin.y === (pomme2.y - 50) && lapin.x === pomme2.x)) {
                } else {
                    lapin.y += 50;
                    //step_rabbit.play();
                }
            }
            break;
        case 4:
            if (lapin.x > 0) {
                if ((lapin.x === (cerise1.x + 50) && lapin.y === cerise1.y)
                    || (lapin.x === (cerise2.x + 50) && lapin.y === cerise2.y)
                    || (lapin.x === (banane1.x + 50) && lapin.y === banane1.y)
                    || (lapin.x === (banane2.x + 50) && lapin.y === banane2.y)
                    || (lapin.x === (poire1.x + 50) && lapin.y === poire1.y)
                    || (lapin.x === (poire2.x + 50) && lapin.y === poire2.y)
                    || (lapin.x === (pineapple1.x + 50) && lapin.y === pineapple1.y)
                    || (lapin.x === (pineapple2.x + 50) && lapin.y === pineapple2.y)
                    || (lapin.x === (raisin1.x + 50) && lapin.y === raisin1.y)
                    || (lapin.x === (raisin2.x + 50) && lapin.y === raisin2.y)
                    || (lapin.x === (fraise1.x + 50) && lapin.y === fraise1.y)
                    || (lapin.x === (fraise2.x + 50) && lapin.y === fraise2.y)
                    || (lapin.x === (citron1.x + 50) && lapin.y === citron1.y)
                    || (lapin.x === (citron2.x + 50) && lapin.y === citron2.y)
                    || (lapin.x === (pomme1.x + 50) && lapin.y === pomme1.y)
                    || (lapin.x === (pomme2.x + 50) && lapin.y === pomme2.y)) {
                } else {
                    lapin.x -= 50;
                    //step_rabbit.play();
                }
            }
            break;
        case 5:
            if (lapin.x < 300) {
                if ((lapin.x === (cerise1.x - 50) && lapin.y === cerise1.y)
                    || (lapin.x === (cerise2.x - 50) && lapin.y === cerise2.y)
                    || (lapin.x === (banane1.x - 50) && lapin.y === banane1.y)
                    || (lapin.x === (banane2.x - 50) && lapin.y === banane2.y)
                    || (lapin.x === (poire1.x - 50) && lapin.y === poire1.y)
                    || (lapin.x === (poire2.x - 50) && lapin.y === poire2.y)
                    || (lapin.x === (pineapple1.x - 50) && lapin.y === pineapple1.y)
                    || (lapin.x === (pineapple2.x - 50) && lapin.y === pineapple2.y)
                    || (lapin.x === (raisin1.x - 50) && lapin.y === raisin1.y)
                    || (lapin.x === (raisin2.x - 50) && lapin.y === raisin2.y)
                    || (lapin.x === (fraise1.x - 50) && lapin.y === fraise1.y)
                    || (lapin.x === (fraise2.x - 50) && lapin.y === fraise2.y)
                    || (lapin.x === (citron1.x - 50) && lapin.y === citron1.y)
                    || (lapin.x === (citron2.x - 50) && lapin.y === citron2.y)
                    || (lapin.x === (pomme1.x - 50) && lapin.y === pomme1.y)
                    || (lapin.x === (pomme2.x - 50) && lapin.y === pomme2.y)) {
                } else {
                    lapin.x += 50;
                    //step_rabbit.play();
                }
            }
            break;
        case 6:
            if (lapin.y > 0) {
                if ((lapin.y === (cerise1.y + 50) && lapin.x === cerise1.x)
                    || (lapin.y === (cerise2.y + 50) && lapin.x === cerise2.x)
                    || (lapin.y === (banane1.y + 50) && lapin.x === banane1.x)
                    || (lapin.y === (banane2.y + 50) && lapin.x === banane2.x)
                    || (lapin.y === (poire1.y + 50) && lapin.x === poire1.x)
                    || (lapin.y === (poire2.y + 50) && lapin.x === poire2.x)
                    || (lapin.y === (pineapple1.y + 50) && lapin.x === pineapple1.x)
                    || (lapin.y === (pineapple2.y + 50) && lapin.x === pineapple2.x)
                    || (lapin.y === (raisin1.y + 50) && lapin.x === raisin1.x)
                    || (lapin.y === (raisin2.y + 50) && lapin.x === raisin2.x)
                    || (lapin.y === (fraise1.y + 50) && lapin.x === fraise1.x)
                    || (lapin.y === (fraise2.y + 50) && lapin.x === fraise2.x)
                    || (lapin.y === (citron1.y + 50) && lapin.x === citron1.x)
                    || (lapin.y === (citron2.y + 50) && lapin.x === citron2.x)
                    || (lapin.y === (pomme1.y + 50) && lapin.x === pomme1.x)
                    || (lapin.y === (pomme2.y + 50) && lapin.x === pomme2.x)) {
                } else {
                    lapin.y -= 50;
                    //step_rabbit.play();
                }
            }
            break;
        case 7:
            if (lapin.y < 500) {
                if ((lapin.y === (cerise1.y - 50) && lapin.x === cerise1.x)
                    || (lapin.y === (cerise2.y - 50) && lapin.x === cerise2.x)
                    || (lapin.y === (banane1.y - 50) && lapin.x === banane1.x)
                    || (lapin.y === (banane2.y - 50) && lapin.x === banane2.x)
                    || (lapin.y === (poire1.y - 50) && lapin.x === poire1.x)
                    || (lapin.y === (poire2.y - 50) && lapin.x === poire2.x)
                    || (lapin.y === (pineapple1.y - 50) && lapin.x === pineapple1.x)
                    || (lapin.y === (pineapple2.y - 50) && lapin.x === pineapple2.x)
                    || (lapin.y === (raisin1.y - 50) && lapin.x === raisin1.x)
                    || (lapin.y === (raisin2.y - 50) && lapin.x === raisin2.x)
                    || (lapin.y === (fraise1.y - 50) && lapin.x === fraise1.x)
                    || (lapin.y === (fraise2.y - 50) && lapin.x === fraise2.x)
                    || (lapin.y === (citron1.y - 50) && lapin.x === citron1.x)
                    || (lapin.y === (citron2.y - 50) && lapin.x === citron2.x)
                    || (lapin.y === (pomme1.y - 50) && lapin.x === pomme1.x)
                    || (lapin.y === (pomme2.y - 50) && lapin.x === pomme2.x)) {
                } else {
                    lapin.y += 50;
                    //step_rabbit.play();
                }
            }
            break;
    }
}
//------------------------ LE JEU ---------------------
function ShowGame() {
    "use strict";
    $("#replay").html("");
    c += 1;
    if (!gameOver) {
        if (c % 2 === 1) {
            console.log('hide');
            $('#switchGame').text('+');
            $('#showHide_game').css('display', 'none');
            $('#gameInfo').css('display', 'none');
            $('#tiLapin').css('display', 'block');
        } else if (c % 2 === 0) {
            console.log('show');
            $('#switchGame').text('-');
            $('#showHide_game').css('display', 'block');
            $('#gameInfo').css('display', 'block');
            $('#tiLapin').css('display', 'none');
        }
    }
}

function Start() {
    "use strict";
    c = 1;
    $("#vfd").css({ 'display': 'block' });
    Respawn();
    lapin = { x: 150, y: 500 };
    renard = { x: 150, y: 0 };
    score = 0;
    gameOver = 0;
    ctx.filter = 'none';
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    $("#score").html(score).css({ 'text-align': 'right', 'color': '#bbffff' });
    $("#gameover").html('GAME OVER').css('color', 'rgb(33, 37, 41)');
    $("#hiscore").html('HISCORE').css('color', 'rgb(33, 37, 41)');
    $("#life").html("<i class='fas fa-heart' style='color:red'></i>");
    ShowGame();
}



function Replay() {
    "use strict";
    $('body').keydown(function (event) {
        if ((event.keyCode === 32) && gameOver) {
            event.preventDefault();
            Start();
        }
        if ((event.keyCode === 13) && gameOver) {
            event.preventDefault();
            Start();
        }
    });
    $('#matrice').click(function (event) {
        if (gameOver) {
            event.preventDefault();
            Start();
        }
    });

}
function Respawn() {
    "use strict";
    var val = Math.floor(Math.random() * 20);
    switch (val) {
        case 0:
            carotte = { x: 0, y: 0 };
            break;
        case 1:
            carotte = { x: 100, y: 0 };
            break;
        case 2:
            carotte = { x: 200, y: 0 };
            break;
        case 3:
            carotte = { x: 300, y: 0 };
            break;
        case 4:
            carotte = { x: 50, y: 100 };
            break;
        case 5:
            carotte = { x: 250, y: 100 };
            break;
        case 6:
            carotte = { x: 150, y: 150 };
            break;
        case 7:
            carotte = { x: 0, y: 200 };
            break;
        case 8:
            carotte = { x: 300, y: 200 };
            break;
        case 9:
            carotte = { x: 100, y: 250 };
            break;
        case 10:
            carotte = { x: 200, y: 250 };
            break;
        case 11:
            carotte = { x: 0, y: 300 };
            break;
        case 12:
            carotte = { x: 300, y: 300 };
            break;
        case 13:
            carotte = { x: 150, y: 350 };
            break;
        case 14:
            carotte = { x: 50, y: 400 };
            break;
        case 15:
            carotte = { x: 250, y: 400 };
            break;
        case 16:
            carotte = { x: 0, y: 500 };
            break;
        case 17:
            carotte = { x: 100, y: 500 };
            break;
        case 18:
            carotte = { x: 200, y: 500 };
            break;
        case 19:
            carotte = { x: 300, y: 500 };
            break;
    }
}
function Manger() {
    "use strict";
    if ((carotte.x === lapin.x) && (carotte.y === lapin.y)) {
        score += 50;
        $("#score").html(score);
        Respawn();
    }
    if ((lapin.x === renard.x) && (lapin.y === renard.y)) {
        lapin = {};
        gameOver = 1;
        $("#gameover").css('color', 'red');
        if (c !== 0) {
            $("#life").html("<i class='fas fa-heart' style='color:rgb(33, 37, 41)'></i>");
        }
        ctx.filter = 'grayscale(100%) brightness(0.5)';
        if (score !== 0 && (score > hiscore)) {
            hiscore = score;
            $("#hiscore").css('color', 'yellow');
        }
        Replay();
    }
}
setInterval(function Refresh() {
    Canvas();
    Manger();
    Carotte();
    Lapin();
    Renard();
    Cerise1();
    Cerise2();
    Banane1();
    Banane2();
    Poire1();
    Poire2();
    Pineapple1();
    Pineapple2();
    Raisin1();
    Raisin2();
    Fraise1();
    Fraise2();
    Citron1();
    Citron2();
    Pomme1();
    Pomme2();
}, 50);

/*

    setInterval(Tactile, 250);

    */